package fr.telecom.paris.plugins;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

public class CheckEvent {
    public static void main(String[] args) {
        Json json = new Json();

        try {
            System.out.println(Paths.get(".").toAbsolutePath());
            String jsonString = new String(Files.readAllBytes(Paths.get("../assets/story/story-fr.json")));
            JsonValue jsonObject = json.fromJson(null, jsonString);

            jsonObject.remove("dates");

            Set<Integer> ids = new HashSet<>();
            for (JsonValue event : jsonObject) {
                String eventName = event.name;
                boolean con = false;

                if (!event.has("id")) {
                    System.out.println("Event '" + eventName + "' is missing the 'id' field.");
                    con = true;
                }
                if (!event.has("type")) {
                    System.out.println("Event '" + eventName + "' is missing the 'type' field.");
                    con = true;
                }
                if (!event.has("subject")) {
                    System.out.println("Event '" + eventName + "' is missing the 'subject' field.");
                    con = true;
                }
                if (!event.has("text")) {
                    System.out.println("Event '" + eventName + "' is missing the 'text' field.");
                    con = true;
                }

                if (con)
                    continue;



                int id = event.getInt("id");
                String type = event.getString("type");
                String subject = event.getString("subject");
                String text = event.getString("text");
                // Check if id is unique
                if (ids.contains(id)) {
                    System.out.println("Event "+eventName+" with id " + id + " is not unique.");
                }
                ids.add(id);

                // Check if type is valid
                if (!type.matches("[mrsaei]")) {
                    System.out.println("Event "+eventName+" with id " + id + " has invalid type.");
                }

                // Check if subject and text are non-empty
                if (subject.isEmpty() || text.isEmpty()) {
                    System.out.println("Event " +eventName+ " with id " + id + " has empty subject or text.");
                }

                // Check if choices are valid
                if (event.has("choices")) {
                    for (JsonValue choiceObj : event.get("choices")) {
                        if (choiceObj.getString("text",null) == null || choiceObj.getString("next",null) == null) {
                            System.out.println("Choice in event "+eventName+" with id " + id + " is missing text or next field.");
                        }
                    }
                }

                // Check if need is valid
                if (event.has("need")) {
                    int need = event.getInt("need");
                    if (!ids.contains(need)) {
                        System.out.println("Event "+eventName+" with id " + id + " has invalid need field.");
                    }
                }

                // Check if randoms are valid
                if (event.has("randoms")) {
                    for (JsonValue randomObj : event.get("randoms")) {
                        String random = randomObj.asString();
                        if (!jsonObject.has(random)) {
                            System.out.println("Event "+eventName+" with id " + id + " has invalid random field.");
                        }
                    }
                }
            }

            System.out.println("All events are correct.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}