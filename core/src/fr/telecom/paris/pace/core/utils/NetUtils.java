package fr.telecom.paris.pace.core.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpParametersUtils;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter;

import java.util.HashMap;
import java.util.Map;

public class NetUtils {

    private static final String SERVER_URL = "https://perso.telecom-paristech.fr/kgarnier-23/pace/register_result.php";

    /**
     * Send a JSON object to the server.
     * @param jsonValue The JSON object to send.
     */

    public static void sendJsonToServer(JsonValue jsonValue) {
        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        Net.HttpRequest request = requestBuilder.newRequest()
                .method(Net.HttpMethods.POST)
                .url(SERVER_URL)
                .content(jsonValue.toJson(JsonWriter.OutputType.json))
                .header("Content-Type", "application/json")
                .build();
        Gdx.app.log("HTTP Request", "Sending JSON: " + jsonValue.toJson(JsonWriter.OutputType.json));


        Gdx.net.sendHttpRequest(request, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                Gdx.app.log("HTTP Response", "Response code: " + httpResponse.getStatus().getStatusCode());
            }

            @Override
            public void failed(Throwable t) {
                Gdx.app.error("HTTP Request", "Request failed", t);
            }

            @Override
            public void cancelled() {
                Gdx.app.log("HTTP Request", "Request cancelled");
            }
        });
    }
}