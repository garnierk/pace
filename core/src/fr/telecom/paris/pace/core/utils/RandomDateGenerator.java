package fr.telecom.paris.pace.core.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class RandomDateGenerator {
    private final GregorianCalendar gc;

    public RandomDateGenerator() {
        gc = new GregorianCalendar();
    }

    public Date nextDate(int startYear, int endYear) {
        int year = randBetween(startYear, endYear);
        gc.set(Calendar.YEAR, year);
        int dayOfYear = randBetween(1, gc.getActualMaximum(Calendar.DAY_OF_YEAR));
        gc.set(Calendar.DAY_OF_YEAR, dayOfYear);
        return gc.getTime();
    }

    private int randBetween(int start, int end) {
        return start + new Random().nextInt(end - start + 1);
    }

    public String DateToString(Date date) {
        gc.setTime(date);
        return gc.get(Calendar.DAY_OF_MONTH) + "/" + (gc.get(Calendar.MONTH) + 1) + "/" + gc.get(Calendar.YEAR);
    }
}