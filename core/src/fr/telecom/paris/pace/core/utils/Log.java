package fr.telecom.paris.pace.core.utils;

import com.badlogic.gdx.Gdx;

import java.io.PrintStream;

public class Log {

    public static void print(String message) {

        Gdx.app.log("Pace", message);

    }

    public static void print(Status status, String message) {

        Gdx.app.log("Pace",status.str + " " + message);

    }
    public enum Status {

        ERROR(-1, "[X]"), SUCCESS(0, "[OK]"), WARN(1, "[!]"), INFO(2, "[i]");

        final int code;
        final String str;

        Status(int code, String str) {

            this.code = code;
            this.str = str;

        }

    }

}
