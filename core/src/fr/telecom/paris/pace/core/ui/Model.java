package fr.telecom.paris.pace.core.ui;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import fr.telecom.paris.pace.core.utils.RandomDateGenerator;

import java.util.*;

public class Model implements Json.Serializable {
    JsonValue story;

    private int year;

    private boolean gameOver;

    private Event currentEvent;

    private ArrayList<Event> randoms;

    private HashSet<Integer> eventsDone;

    private float stability;

    private float liberty;

    private float propaganda;

    private HashMap<Integer,String> dates;

    private RandomDateGenerator rdDate;

    private PlayerEndState playerEndState;

    private static final int END_YEAR = 2024;

    private int difficulty = 2;

    public Model() {
        this.stability = -1;
        this.liberty = -1;
        this.year = -1;
        this.propaganda = -1;
        randoms = new ArrayList<>();
        eventsDone = new HashSet<>();
        dates = new HashMap<>();
        rdDate = new RandomDateGenerator();
        gameOver = false;
    }
    public void dispose() {
    }

    public void startGame() {
        if (year < 0) {
            year = 1988;
        }
        if (liberty < 0) {
            liberty = 0.5f;
        }

        if (stability < 0) {
            stability = 0.7f;
        }

        if (propaganda < 0) {
            propaganda = 0.7f;
        }
        load_story();
        nextYear();

    }

    public int getYear() {
        return year;
    }

    public String getYearString() {
        if (gameOver)
            return "Fin";
        return Integer.toString(year);

    }

    public void nextYear() {
        year++;
        String event_name = dates.get(year);
        if (event_name != null) {
            loadEvent(event_name);
        } else {
            Random random = new Random();
            currentEvent = randoms.get(random.nextInt(randoms.size()));
        }
        stability += currentEvent.stability* difficulty;
        liberty += currentEvent.liberty*difficulty;
        propaganda += currentEvent.propaganda*difficulty;
        notifyCountryStatus();
        checkGameStatus();


    }

    private void checkGameStatus() {
        if (stability <= 0 || liberty >=1) {
            end_of_game();
            return;
        }
        if (liberty <= 0) {
            end_of_liberty();
        }
        if (propaganda <= 0) {
            end_of_propaganda();
        }

        if (propaganda >= 1 && liberty >= 0.7) {
            paradox();
        }
    }

    public void nextYear(EventChoices choice) {
        year++;
        loadEvent(choice.event_name);
        eventsDone.add(currentEvent.id);
        stability += currentEvent.stability;
        liberty += currentEvent.liberty;
        propaganda += currentEvent.propaganda;
        notifyCountryStatus();
        checkGameStatus();
        cleanEvents();
    }

    private void cleanEvents() {
        for (Integer remove : currentEvent.removes) {
            eventsDone.remove(remove);
        }
        for (Event random : randoms) {
            if (!eventsDone.contains(random.need) && random.need != -1){
                eventsDone.remove(random.id);
            }
        }
        randoms.removeIf(event -> !eventsDone.contains(event.need) && event.need != -1);
    }

    private void paradox() {
        currentEvent.content += "\n"+Assets.getInstance().getText("paradox");
        if (stability <= 0.3) {
            stability -= 0.5f;
            currentEvent.content += "\n"+Assets.getInstance().getText("stability_down");
        }
    }

    private void end_of_propaganda() {
        currentEvent.content += "\n"+Assets.getInstance().getText("end_of_propaganda");
        if (liberty <= 0.3) {
            stability -= 0.5f;
            currentEvent.content += "\n"+Assets.getInstance().getText("stability_down");
        }
    }

    private void end_of_liberty() {
        currentEvent.content += "\n"+Assets.getInstance().getText("end_of_liberty");
        if (propaganda <= 0.8) {
            stability -= 0.5f;
            currentEvent.content += "\n"+Assets.getInstance().getText("stability_down");
        }

    }

    private void end_of_game() {
        if (stability <= 0) {
            loadEvent("end_of_the_game_fall");
            playerEndState = PlayerEndState.LOSE_STABILITY;
            gameOver = true;
            return;
        }
        if (liberty >= 1) {
            loadEvent("end_of_the_game_liberty");
            playerEndState = PlayerEndState.LOSE_LIBERTY;
            gameOver = true;
            return;
        }
        if (year == END_YEAR) {
            loadEvent("end_of_the_game_time");
            playerEndState = PlayerEndState.WIN;
            gameOver = true;
        }
    }

    public float getLiberty() {
        return liberty;
    }

    public float getPropaganda() {
        return propaganda;
    }

    public float getStability() {
        return stability;
    }

    private void notifyCountryStatus() {
        if (stability >= 1) {
            currentEvent.content += "\n"+Assets.getInstance().getText("stability_max");
        }
        if (stability <= 0) {
            currentEvent.content += "\n"+Assets.getInstance().getText("stability_min");
        }
        if (liberty >= 1) {
            currentEvent.content += "\n"+Assets.getInstance().getText("liberty_max");
        }
        if (liberty <= 0) {
            currentEvent.content += "\n"+Assets.getInstance().getText("liberty_min");
        }
        if (propaganda >= 1) {
            currentEvent.content += "\n"+Assets.getInstance().getText("propaganda_max");
        }
        if (propaganda <= 0) {
            currentEvent.content += "\n"+Assets.getInstance().getText("propaganda_min");
        }
    }

    public String getSecretDocumentSubject() {
        return "Secret Document";
    }

    public String getSecretDocumentContent() {
        return "This is a secret document";
    }

    public void load_story() {
        // Load the story
        story = Assets.getInstance().getStory();
        for (JsonValue event : story.get("dates")) {
            dates.put(event.getInt("year"), event.getString("name"));
        }

    }

    public void loadEvent(String event) {
        JsonValue event_json = story.get(event);
        currentEvent = loadEvent(event_json);
    }

    public Event loadEvent(JsonValue event) {
        String subject = event.getString("subject");
        String content = event.getString("text");
        float stability = 0;
        float liberty = 0;
        float propaganda = 0;
        ArrayList<EventChoices> choices = new ArrayList<>();
        ArrayList<Integer> removes = new ArrayList<>();
        int id = event.getInt("id");
        String next = null;
        if (event.has("next")) {
            next = event.getString("next");
        }
        int need = -1;
        if (event.has("need")) {
            need = event.getInt("need");
        }
        char type = event.getChar("type");
        if (event.has("choices")) {
            for (JsonValue choice : event.get("choices")) {
                choices.add(new EventChoices(choice.getString("text"), choice.getString("next", "")));
            }
        }
        if (event.has("stability")) {
            stability += event.getFloat("stability");
        }
        if(event.has("liberty")) {
            liberty += event.getFloat("liberty");
        }
        if (event.has("propaganda")) {
            propaganda += event.getFloat("propaganda");
        }
        if (event.has("randoms")) {
            for (JsonValue random : event.get("randoms")) {
                JsonValue random_event = story.get(random.toString());
                randoms.add(loadEvent(random_event));
            }
        }
        if (event.has("removes")) {
            for (JsonValue remove : event.get("removes")) {
                removes.add(remove.asInt());
            }
        }
        return new Event(id, need, subject, content, stability, liberty, propaganda, next, choices, type, removes);
    }

    public String getContent() {
        return currentEvent.getContent();
    }

    public String getDate() {
        return rdDate.DateToString(rdDate.nextDate(year,year));

    }

    public ArrayList<EventChoices> getChoices() {
        return currentEvent.choices;
    }

    public String getSubject() {
        return currentEvent.getSubject();
    }

    public Event getCurrentEvent() {
        return currentEvent;
    }

    public void loadNewEvent() {
        String event_name = dates.get(year);
        if (event_name != null) {
            loadEvent(event_name);
        } else {
            Random random = new Random();
            currentEvent = randoms.get(random.nextInt(randoms.size()));
        }
        stability += currentEvent.stability * difficulty;
        liberty += currentEvent.liberty * difficulty;
        propaganda += currentEvent.propaganda * difficulty;
        notifyCountryStatus();
        if (stability <= 0 || liberty >=1) {
            end_of_game();
        }
        if (liberty <= 0) {
            end_of_liberty();
        }
        if (propaganda <= 0) {
            end_of_propaganda();
        }

        if (propaganda >= 1 && liberty >= 0.7) {
            paradox();
        }
    }


    public HashSet<Integer> getEventsDone() {
        return eventsDone;
    }

    public float getOldStability() {
        return stability - currentEvent.stability;
    }

    public float getOldLiberty() {
        return liberty - currentEvent.liberty;
    }

    public float getOldPropaganda() {
        return propaganda - currentEvent.propaganda;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    @Override
    public void write(Json json) {
        json.writeValue("year", year);
        json.writeValue("stability", stability);
        json.writeValue("liberty", liberty);
        json.writeValue("propaganda", propaganda);
        json.writeValue("current_event", currentEvent);
        json.writeValue("randoms", randoms);
        json.writeValue("events_done", eventsDone);
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        year = jsonData.getInt("year");
        stability = jsonData.getFloat("stability");
        liberty = jsonData.getFloat("liberty");
        propaganda = jsonData.getFloat("propaganda");
        currentEvent = json.readValue(Event.class, jsonData.get("current_event"));
        randoms = json.readValue(ArrayList.class, jsonData.get("randoms"));
        eventsDone = json.readValue(HashSet.class, jsonData.get("events_done"));
        dates = new HashMap<>();
        load_story();
    }

    public int getPlayerEndState() {
        switch (playerEndState) {
            case LOSE_LIBERTY:
                return 1;
            case LOSE_STABILITY:
                return 2;
            case WIN:
                return 3;
            default:
                return 0;
        }
    }

    public static class Event {
        private final String subject;
        private String content;

        private final float stability;

        private final float liberty;

        private final float propaganda;

        private final int id;

        private final int need;
        private final String next;

        private ArrayList<EventChoices> choices;

        private final EventTypes type;

        private ArrayList<Integer> removes;

        public Event() {
            this.subject = "";
            this.content = "";
            this.stability = 0;
            this.liberty = 0;
            this.propaganda = 0;
            this.id = 0;
            this.need = 0;
            this.next = "";
            this.choices = new ArrayList<>();
            this.type = EventTypes.MAJOR;
            this.removes = new ArrayList<>();
        }

        public Event(int id, int need, String subject, String content, float stability, float liberty, float propaganda, String next, ArrayList<EventChoices> choices, char type, ArrayList<Integer> removes) {
            this.subject = subject;
            this.content = content + "\n";
            this.stability = stability;
            this.liberty = liberty;
            this.propaganda = propaganda;
            this.id = id;
            this.need = need;
            this.next = next;
            this.choices = choices;
            this.type = compute_type(type);
            this.removes = removes;
            notify_modifications();
        }

        private void notify_modifications() {
            if (stability > 0) {
                this.content += "\n" + Assets.getInstance().getText("stability_up");
            } else if (stability < 0) {
                this.content += "\n" + Assets.getInstance().getText("stability_down");
            }
            if (liberty > 0) {
                this.content += "\n" + Assets.getInstance().getText("liberty_up");
            } else if (liberty < 0) {
                this.content += "\n" + Assets.getInstance().getText("liberty_down");
            }
            if (propaganda > 0) {
                this.content += "\n" + Assets.getInstance().getText("propaganda_up");
            } else if (propaganda < 0) {
                this.content += "\n" + Assets.getInstance().getText("propaganda_down");
            }
        }

        public String getContent() {
            return content;
        }

        public String getSubject() {
            return subject;
        }

        public EventTypes getType() {
            return type;
        }

        private EventTypes compute_type(char type) {
            switch (type) {
                case 'm':
                    return EventTypes.MAJOR;

                case 'r':
                    return EventTypes.RANDOM;

                case 's':
                    return EventTypes.SPECIAL;

                case 'a':
                    return EventTypes.ANSWER;
                case 'i':
                    return EventTypes.INTRODUCTION;
                case 'e':
                    return EventTypes.END;
                default:
                    return null;
            }


        }

        public boolean hasChoices() {
            return !choices.isEmpty();
        }
    }

    public static class EventChoices {
        private final String text;
        private final String event_name;

        private EventChoices() {
            this.text = "";
            this.event_name = "";
        }

        private EventChoices(String text, String event_name) {
            this.text = text;
            this.event_name = event_name;
        }

        public String getText() {
            return text;
        }
    }

    public enum EventTypes {
        MAJOR,
        RANDOM,
        SPECIAL,
        ANSWER,
        END,
        INTRODUCTION
    }

    public enum PlayerEndState {
        WIN,
        LOSE_LIBERTY,
        LOSE_STABILITY,

    }
}
