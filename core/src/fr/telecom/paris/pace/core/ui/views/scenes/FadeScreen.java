package fr.telecom.paris.pace.core.ui.views.scenes;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.PaceClient;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Interpolation;

public class FadeScreen extends View {
    private ShapeRenderer shapeRenderer;
    private OrthographicCamera camera;

    private View screen;
    private View next;
    private FadeInfo fade;
    private float elapsed;

    public FadeScreen(Controller controller, Model model, PaceClient pace, View current, View next, FadeInfo fade) {
        super(controller, model, pace);
        this.next = next;
        this.fade = fade;
        this.screen = current;

    }

    @Override
    public void init() {
        shapeRenderer = new ShapeRenderer();
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(camera.viewportWidth / 2.0f, camera.viewportHeight / 2.0f, 0.0f);
        camera.update();

    }

    public enum FadeType {IN, OUT}

    public static class FadeInfo {
        public final FadeType type;
        public final Color color;
        public final Interpolation interpolation;
        public final float duration;

        public FadeInfo(FadeType type, Color color, Interpolation interpolation, float duration) {
            this.type = type;
            this.color = color;
            this.interpolation = interpolation;
            this.duration = duration;
        }
    }


    private void renderFade() {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        float f = Math.min(1.0f, elapsed / fade.duration);
        float opacity = fade.type == FadeType.OUT ? fade.interpolation.apply(f) : 1.0f - fade.interpolation.apply(f);

        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(fade.color.r, fade.color.g, fade.color.b, opacity);
        shapeRenderer.rect(0, 0, camera.viewportWidth, camera.viewportHeight);
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    public void render(float delta) {
        if (screen != null) {
            elapsed += delta;
            if (elapsed >= fade.duration) {
                if (next != null) {
                    getClient().setScreen(next,false);
                } else
                    getClient().setScreen(next,false);
            }
        }

        Gdx.gl.glClearColor(0, 0, 0, 0);
        if (screen != null)
            screen.render(delta);
        renderFade();
    }

    @Override
    public void draw(float delta) {

    }
}