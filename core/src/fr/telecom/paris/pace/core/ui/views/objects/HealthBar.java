package fr.telecom.paris.pace.core.ui.views.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import fr.telecom.paris.pace.core.utils.Drawer;

/**
 * The progress bar which reassembles the behaviour of the health bar.
 * 
 * @author serhiy
 */
public class HealthBar extends ProgressBar {

	/**
	 * @param width of the health bar
	 * @param height of the health bar
	 */
	public HealthBar(int width, int height, float oldValue, float newValue) {
		super(0f, 1f, 0.01f, false, new ProgressBarStyle());
		getStyle().background = Drawer.getColoredDrawable(width, height, Color.RED);
		getStyle().knob = Drawer.getColoredDrawable(0, height, Color.GREEN);
		getStyle().knobBefore = Drawer.getColoredDrawable(width, height, Color.GREEN);
		
		setWidth(width);
		setHeight(height);

		setAnimateDuration(0.0f);
		setValue(oldValue);

		setAnimateDuration(1f);
		setValue(newValue);
	}
}
