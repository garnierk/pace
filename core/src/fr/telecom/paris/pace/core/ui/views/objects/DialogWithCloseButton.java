package fr.telecom.paris.pace.core.ui.views.objects;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import fr.telecom.paris.pace.core.ui.Assets;
import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.views.scenes.GameView;

public abstract class DialogWithCloseButton extends Dialog {
    private final CheckBox exitBox;

    private final Controller controller;

    /**
     * Creates a dialog with an exit button
     *
     * @param title the name of the dialog
     */
    public DialogWithCloseButton(String title, Controller controller) {
        super("", Assets.getInstance().get(Assets.AssetDir.SKIN.path() + "ui.json", Skin.class));
        this.controller = controller;
        DialogWithCloseButton dialog = this;
        exitBox = new CheckBox("", Assets.getInstance().get(Assets.AssetDir.SKIN.path() + "ui.json", Skin.class));
        setClip(false);
        setTransform(true);
        getTitleTable().add(exitBox).padRight(10).padTop(-exitBox.getHeight());
        exitBox.setChecked(true);
        exitBox.addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        controller.playSound(Assets.getInstance().getSound());
                        controller.dialogClosed(dialog);
                        exitBox.setChecked(true);
                    }
                });
        this.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                TextButton menuButton = ((GameView) controller.getScreen()).getMenuButton();
                int fix_y = 50;
                System.out.println("x: " + x + " y: " + y + "\n x_min: " + (menuButton.getX() - dialog.getX()) + " y_min: " + (menuButton.getY() - menuButton.getHeight() - getY() + fix_y) + " x_max: " + (menuButton.getX() + menuButton.getWidth() - dialog.getX()) + " y_max: " + (menuButton.getY() - getY() + 50));
                if (x >= menuButton.getX() - getX() && y >= menuButton.getY() - menuButton.getHeight() - getY() + fix_y && x <= menuButton.getX() + menuButton.getWidth() - dialog.getX() && y <= menuButton.getY() - getY() + fix_y) {
                    menuButton.setChecked(true);
                    System.out.println("x: " + x + " y: " + y + " button: " + button + " pointer: " + pointer + " menuButton: " + menuButton.getX() + " " + menuButton.getY() + " " + menuButton.getWidth() + " " + menuButton.getHeight());
                    return true;
                }
                if (x < 0 || y < 0 || x > getWidth() || y > getHeight()) {
                    System.out.println("x: " + x + " y: " + y);
                    controller.dialogClosed(dialog);
                    return true;
                }
                return false;
            }
        });
        init();
        initInput();
        update();

    }

    public abstract void init();

    public abstract void initInput();

    public abstract void update();

    /***************** setters and getters *****************/

    public CheckBox getCloseButtonMenu() {
        return exitBox;
    }

    public Controller getController() {
        return controller;
    }

}
