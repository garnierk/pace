package fr.telecom.paris.pace.core.ui.views.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import fr.telecom.paris.pace.core.ui.Assets;
import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.PaceClient;
import fr.telecom.paris.pace.core.ui.views.scenes.MainMenu;
import fr.telecom.paris.pace.core.ui.views.scenes.View;

public class GameOverScreen extends View {
    public GameOverScreen(Controller controller, Model model, PaceClient client) {
        super(controller, model, client);
    }

    private TextButton mainMenu;
    private Camera camera;

    private Table table;


    /**
     * Initialize the screen and its components.
     */
    @Override
    public void init() {
        Skin skin = Assets.getInstance().getSkin();
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        setStage(new ScreenViewport(camera));
        mainMenu = new TextButton("Main Menu", skin);
        addActor(initTable());
        initListener();
        setBackground(Assets.getInstance().get(Assets.AssetDir.BACKGROUND.path() + "dictator_office.jpeg", Texture.class));
    }

    private Table initTable() {
        table = new Table();
        table.setFillParent(true);
        table.add(mainMenu).expand();
        return table;
    }

    private void initListener() {
        mainMenu.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getController().playSound(Assets.getInstance().getSound());
                getController().stopGame(false);
                getController().openMainMenu();
            }
        });
    }

    /**
     * Draw the screen.
     *
     * @param delta The number of second that have passed since the last frame
     */
    @Override
    public void draw(float delta) {
        getBatch().setTransformMatrix(camera.view);
        getBatch().setProjectionMatrix(camera.projection);
        getBatch().begin();
//        getBatch().setColor(0.5f,0.5f, 0.5f, 1f);
        getBackground().setColor(0.5f, 0.5f, 0.5f, 1f);
        getBackground().draw(getBatch());
//        getBatch().setColor(1,1,1, 1f);
        getBatch().end();
        getStage().draw();

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        table.setSize(width / 4.0f, height / 4.0f);
        for (Cell cell : table.getCells()) {
            cell.size(table.getWidth(), table.getHeight() / 4.0f);
            if (cell.getActor() instanceof TextButton)
                ((TextButton) cell.getActor()).getLabel().setFontScale(table.getScaleX(), table.getScaleY());
        }

        camera.update();
//        table.scaleBy(scale);
        getBackground().setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }
}