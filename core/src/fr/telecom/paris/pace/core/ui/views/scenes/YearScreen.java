package fr.telecom.paris.pace.core.ui.views.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.PaceClient;

public class YearScreen extends View{

    private Camera camera;

    private Label year;
    private Table table;

    public YearScreen(Controller controller, Model model, PaceClient pace) {
        super(controller, model, pace);
    }

    @Override
    public void init() {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Viewport viewport = new ScreenViewport(camera);
        Skin skin = getAssets().getSkin();
        year = new Label(getModel().getYearString(), skin);
        setStage(viewport);
        table = new Table();
        table.center();
        table.setFillParent(true);
        table.add(year).expand().center();
        addActor(table);
        table.setWidth(Gdx.graphics.getWidth() * 0.5f);
        table.setHeight(Gdx.graphics.getHeight() * 0.5f);
        Gdx.input.setInputProcessor(getStage());
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        table.setSize(width, height);
        year.setFontScale(2*table.getScaleX(),2*table.getScaleY());

    }

    @Override
    public void draw(float delta) {
        getBatch().setTransformMatrix(camera.view);
        getBatch().setProjectionMatrix(camera.projection);
        getStage().act(delta);

        getStage().draw();
    }
}
