package fr.telecom.paris.pace.core.ui.views.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import fr.telecom.paris.pace.core.ui.Assets;
import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.PaceClient;

public class MainMenu extends View {

    private Button startButton;

    private Button settingsButton;

    private Button loadButton;

    private Button exitButton;

    private Camera camera;

    private Table table;

    private float old_height = Gdx.graphics.getHeight();

    private float old_width = Gdx.graphics.getWidth();

    public MainMenu(Controller controller, Model model, PaceClient pace) {
        super(controller, model, pace);
    }

    @Override
    public void init() {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        Viewport viewport = new ScreenViewport(camera);
        Skin skin = getAssets().getSkin();
        startButton = new TextButton(getAssets().getText("start"), skin);
        settingsButton = new TextButton(getAssets().getText("settings"), skin);
        exitButton = new TextButton(getAssets().getText("exit"), skin);
        loadButton = new TextButton(getAssets().getText("load"), skin);
        initListener();
        Sprite backgroundSprite = new Sprite(new Texture(Gdx.files.internal("background/doors.jpeg")));
        backgroundSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        setBackground(backgroundSprite);
        setBackground(Assets.getInstance().get(Assets.AssetDir.BACKGROUND.path() + "doors.jpeg", Texture.class));

        setStage(viewport);
        addActor(drawTable());


        Gdx.input.setInputProcessor(getStage());

    }

    @Override
    public void draw(float delta) {
        getBatch().setTransformMatrix(camera.view);
        getBatch().setProjectionMatrix(camera.projection);
        getBatch().begin();
        getBackground().draw(getBatch());
        getBatch().end();

        getStage().draw();
    }

    private Table drawTable() {
        table = new Table();
        table.setFillParent(true);
        table.center();
        table.setTransform(true);

        // Set the size of the table to a fraction of the screen size
        table.setWidth(Gdx.graphics.getWidth() * 0.5f);
        table.setHeight(Gdx.graphics.getHeight() * 0.5f);

        // Set the size, padding and style of each button
        table.add(startButton).pad(30).size(table.getWidth(), table.getHeight() / 4.0f);
        table.row();

        table.add(loadButton).pad(30).size(table.getWidth(), table.getHeight() / 4.0f);
        table.row();

        table.add(settingsButton).pad(30).size(table.getWidth(), table.getHeight() / 4.0f);
        table.row();

        table.add(exitButton).pad(30).size(table.getWidth(), table.getHeight() / 4.0f);


        return table;
    }

    private void initListener() {
        startButton.addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        getController().playSound(Assets.getInstance().getSound());
                        getController().startGame();
                    }
                }
        );

        loadButton.addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        getController().playSound(Assets.getInstance().getSound());
                        getController().showLoadScreen();
                    }
                }
        );


        settingsButton.addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        getController().playSound(Assets.getInstance().getSound());
                        getController().openSettings();
                    }
                }
        );
        exitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getController().playSound(Assets.getInstance().getSound());
                getController().closeClient();
            }
        });

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        if (old_height == 0) {
            old_height = height;
        }

        if (old_height == 0) {
            old_width = width;
        }
        table.setSize(width / 4.0f, height / 4.0f);
        for(Cell cell : table.getCells()) {
            ((TextButton) cell.getActor()).getLabel().setFontScale(table.getScaleX(), table.getScaleY());
        }

        startButton.setSize(width / 2.0f, height / 2.0f);
        camera.update();
        float scale = Math.min(width / old_width, height / old_height);
        old_height = height;
        old_width = width;
//        table.scaleBy(scale);
        getBackground().setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

    }
}
