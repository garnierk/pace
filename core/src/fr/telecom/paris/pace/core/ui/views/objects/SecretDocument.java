package fr.telecom.paris.pace.core.ui.views.objects;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import fr.telecom.paris.pace.core.ui.Assets;
import fr.telecom.paris.pace.core.ui.Model;

public class SecretDocument extends Table {
    private Label title;
    private Label subject;
    private Label date;
    private Label content;
    private Table table;

    private Camera camera;

    Dialog dialog;


    public SecretDocument() {
        super();
        init();
    }
    public void init() {
        setFillParent(true);
        Skin skin = Assets.getInstance().getSkin();
        setColor(Color.WHITE);
        title = new Label("Top Secret Document", skin);
        subject = new Label("Subject: ", skin);
        date = new Label("Date: ", skin);
        content = new Label("Content: ", skin);
        date.setColor(0,0,0,1);
        content.setColor(0,0,0,1);
        title.setColor(0,0,0,1);
        subject.setColor(0,0,0,1);
        content.setWrap(true);
        content.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent pulvinar pretium nisi, vitae laoreet elit lacinia ac. Maecenas mollis congue sem et dapibus. Nulla fringilla sollicitudin urna, a porttitor risus ultricies a. Proin commodo at dui sed congue. Aliquam facilisis dui in consequat luctus. Mauris sit amet sapien ut purus porta venenatis vitae vitae orci. Donec viverra, ex ut pulvinar ornare, purus quam rutrum lorem, sit amet lobortis tellus nisl sit amet risus. Integer ac justo ac quam venenatis gravida sit amet id justo.\n" +
                "\n" +
                "Morbi tincidunt purus at elit ornare, non lacinia odio accumsan. In a odio sapien. Maecenas at ex finibus, sodales lacus et, commodo neque. Praesent euismod ante sit amet nibh auctor vehicula. Praesent dui ex, consequat vel est id, consequat congue ante. Sed ante lectus, imperdiet vel vehicula non, tristique sed nisi. Ut convallis porta convallis. Suspendisse sit amet pretium enim. Etiam tempus felis feugiat neque vulputate dapibus.\n" +
                "\n" +
                "Aliquam quis nunc sit amet nisl finibus ultricies. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam sagittis, magna eu sagittis accumsan, orci ipsum gravida erat, id volutpat nibh odio placerat justo. Praesent blandit ultrices quam vel euismod. Quisque sit amet pretium magna, eu vestibulum diam. In faucibus posuere arcu, ac faucibus nisi volutpat non. Nullam commodo porttitor turpis, sed convallis tortor tincidunt non.\n" +
                "\n" +
                "Proin sollicitudin et est ac commodo. Sed in dui eget turpis gravida bibendum. Maecenas gravida suscipit mauris nec volutpat. Etiam a sem quis tellus pretium ultrices. Nunc eget pretium ligula. Donec orci leo, malesuada vel nisl eget, posuere eleifend mi. Donec tempor, lacus ut blandit efficitur, erat nibh maximus velit, sit amet gravida justo erat eget nibh. Vestibulum id elementum est. Etiam magna ligula, vestibulum nec mattis quis, blandit sit amet tellus. Cras quis lorem imperdiet, fringilla mauris in, imperdiet arcu. Aenean volutpat velit lorem, vitae dignissim ex posuere sit amet. Mauris egestas auctor mi a tincidunt. Phasellus quis mattis purus. In tincidunt odio nec risus varius, at semper mauris porttitor. Pellentesque laoreet sed lectus quis tincidunt. In eu diam quam.\n" +
                "\n" +
                "Sed congue leo magna. Praesent interdum semper lacus sed sodales. Fusce porta ante et pellentesque elementum. Nulla elementum eros sit amet nisl pellentesque, vel dapibus nibh aliquam. Vivamus risus neque, iaculis id ligula et, mattis pretium mi. Nulla ut eros lobortis, mattis urna vel, tristique urna. Duis iaculis enim ac dui vestibulum interdum. Phasellus in ultricies purus, nec luctus mauris. Morbi vel dolor ligula. Integer quis turpis eget urna venenatis eleifend. Quisque sit amet venenatis odio. Donec convallis tincidunt faucibus. Sed at interdum tellus, in mattis lacus. Donec auctor dictum augue id porta.");

        // Underline the subject
        subject.getStyle().font.getData().markupEnabled = true;

        table = new Table();
        table.setFillParent(true);

        // Add the title, subject, date, and content to the table
        table.add(title).expandX().top().pad(50);
        table.row();
        table.add(subject).expandX().top().pad(50).left();
        table.row();
        table.add(date).expandX().top().pad(50).left();
        table.row();
        table.add(content).expand().fill().pad(50);

        }

    public void update(Model model) {
        title.setText("Top Secret Document");
        date.setText("Date: " + model.getYearString());
        subject.setText("Subject: " + model.getSecretDocumentSubject());
        content.setText(model.getSecretDocumentContent());

    }

//    public void render(float delta) {
//        Gdx.gl.glClearColor(1, 1, 1, 1);
//        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//        draw(delta);
//    }

    public void resize(int width, int height) {
        title.setFontScale(table.getScaleX(),table.getScaleY());
        subject.setFontScale(table.getScaleX(),table.getScaleY());
        date.setFontScale(table.getScaleX(),table.getScaleY());
        content.setFontScale(table.getScaleX(),table.getScaleY());
    }
}
