package fr.telecom.paris.pace.core.ui.views.scenes;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import fr.telecom.paris.pace.core.ui.Assets;
import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.PaceClient;

/**
 * Represent a basic screen.
 *
 * @see com.badlogic.gdx.Screen
 */
public abstract class View extends ScreenAdapter {

    /**
     * Screen controller.
     */
    private final Controller controller;
    /**
     * UI model.
     */
    private final Model model;
    /**
     * The client.
     */
    private final PaceClient pace;
    /**
     * The main stage of the screen.
     */
    private Stage ui;

    /**
     * The Screen input manager.
     */
    private InputMultiplexer inputs;

    private Sprite background;


    /**
     * Constructor for View
     *
     * @param controller The screen controller.
     * @param model      The gui model.
     * @param pace       The client.
     */
    public View(Controller controller, Model model, PaceClient pace) {
        this.controller = controller;
        this.model = model;
        this.pace = pace;
    }

    /**
     * Initialize a screen without a controller. <br>
     * <b> A Controller must be set later.</b>
     *
     * @param model The gui model.
     * @param pace  The client.
     */
    public View(Model model, PaceClient pace) {
        this(null, model, pace);
    }

    @Override
    public void show() {
        inputs = new InputMultiplexer();
        this.ui = new Stage();
        inputs.addProcessor(ui);
        Gdx.input.setInputProcessor(inputs);
        init();
    }

    /**
     * Initialize the screen and its components.
     */
    public abstract void init();

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        draw(delta);
    }

    /**
     * Draw the screen.
     *
     * @param delta The number of second that have passed since the last frame
     */
    public abstract void draw(float delta);

    @Override
    public void resize(int width, int height) {
        ui.getViewport().update(width, height, true);
    }

    @Override
    public void dispose() {
        if (ui != null) {
            ui.dispose();
        }
        ui = null;
        inputs = null;
    }

    public Model getModel() {
        return model;
    }

    public Controller getController() {
        return controller;
    }

    /**
     * Sets the main stage of the view.
     *
     * @param viewport The stage's viewport.
     * @return the created stage.
     */
    public Stage setStage(Viewport viewport) {
        this.ui = new Stage(viewport, getBatch());
        inputs.addProcessor(ui);
        return ui;
    }

    public Assets getAssets() {
        return pace.getAssets();
    }


    /**
     * Puts the view in debug mode.
     *
     * @param debug if true, the app is in debug mode
     */
    public void setDebug(boolean debug) {
        ui.setDebugAll(debug);
    }


    public InputMultiplexer getInputs() {
        return inputs;
    }

    /**
     * Adds the given inputs to the app screen input processor.
     *
     * @param inputs The list of inputs that will be added.
     */
    public void addInput(InputProcessor... inputs) {
        for (InputProcessor input : inputs) {
            this.inputs.addProcessor(input);
        }
    }

    /***************** setters and getters *****************/

    /**
     * Adds an actor to the stage.
     *
     * @param actor the actor that will be added
     */
    public void addActor(Actor actor) {
        ui.addActor(actor);
    }

    public PaceClient getClient() {
        return pace;
    }

    public Batch getBatch() {
        return pace.getBatch();
    }

    public Stage getStage() {
        return ui;
    }

    public void setBackground(Sprite background) {
        this.background = background;

    }

    public void setBackground(Texture background) {
        this.background = new Sprite(background);

    }
    public Sprite getBackground() {
        return background;
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }



}
