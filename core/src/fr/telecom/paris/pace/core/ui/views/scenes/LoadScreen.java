package fr.telecom.paris.pace.core.ui.views.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import fr.telecom.paris.pace.core.ui.Assets;
import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.PaceClient;
import fr.telecom.paris.pace.core.ui.views.scenes.View;
import fr.telecom.paris.pace.core.utils.Drawer;
import sun.management.spi.PlatformMBeanProvider;

public class LoadScreen extends View {

    private TextButton back;

    private TextButton load;

    private Camera camera;

    private Table table;
    String date;
    int year;

    boolean hasGameLoaded;


    public LoadScreen(Controller controller, Model model, PaceClient client) {
        super(controller, model, client);
        hasGameLoaded = false;
        date = "";
        year = -1;
    }

    public LoadScreen(Controller controller, Model model, PaceClient client, boolean hasGameLoaded, String date, int year) {
        super(controller, model, client);
        this.hasGameLoaded = hasGameLoaded;
        this.date = date;
        this.year = year;
    }





    @Override
    public void init() {
        Skin skin = Assets.getInstance().getSkin();
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        setStage(new ScreenViewport(camera));
        back = new TextButton(Assets.getInstance().getText("back"), skin);
        load = new TextButton(Assets.getInstance().getText("load"), skin);
        addActor(initTable());
        initListener();
        setBackground(new Texture(Gdx.files.internal("background/doors.jpeg")));

    }

    private void initListener() {
        back.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getController().playSound(Assets.getInstance().getSound());
                getController().openMainMenu();
            }
        });
        if (this.hasGameLoaded) {
            load.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    getController().playSound(Assets.getInstance().getSound());
                    getController().loadGame();
                }
            });
        }
    }

    private Table initTable() {
        table = new Table();
        table.setFillParent(true);
        Table loadingTable = new Table();
        if (this.hasGameLoaded) {

            Texture texture = Assets.getInstance().get(Assets.AssetDir.BACKGROUND.path() + "dictator_office.jpeg", Texture.class);
            Image mini = new Image(texture);
            Label yearLabel = new Label(Integer.toString(year), Assets.getInstance().getSkin());
            yearLabel.setAlignment(1);
            Label dateLabel = new Label(date, Assets.getInstance().getSkin());
            dateLabel.setAlignment(1);
            loadingTable.add(mini).expand().pad(10);
            loadingTable.add(yearLabel).expand().pad(10);
            loadingTable.row();
            loadingTable.add().expand().pad(10);
            loadingTable.add(dateLabel).expand().pad(10);

        } else {
            TextButton loading = new TextButton(Assets.getInstance().getText("no_game_loaded"), Assets.getInstance().getSkin());
            loadingTable.add(loading).expand().fill().pad(10).size(Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 4.0f);
            loading.setDisabled(true);
        }
        Table buttons = new Table();
        buttons.add(back).expand().pad(10).fill();
        if (this.hasGameLoaded) {
            buttons.add().expand();
            buttons.add(load).expand().pad(10).fill();
        }

        table.add(loadingTable).center().expand().pad(10);
        table.row().bottom();
        table.add(buttons).expand().center().fillX().pad(10);

        return table;
    }

    @Override
    public void draw(float delta) {
        getBatch().setTransformMatrix(camera.view);
        getBatch().setProjectionMatrix(camera.projection);
        getBatch().begin();
//        getBatch().setColor(0.5f,0.5f, 0.5f, 1f);
        getBackground().setColor(0.2f, 0.2f, 0.2f, 1f);
        getBackground().draw(getBatch());
//        getBatch().setColor(1,1,1, 1f);
        getBatch().end();
        getStage().draw();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        camera.update();
        resizeTable(table, width, height);


//        table.scaleBy(scale);
        getBackground().setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    private void resizeTable(Table table, int width, int height) {
        table.setSize(width / 4.0f, height / 4.0f);
        for (Cell cell : table.getCells()) {
            cell.size(table.getWidth(), table.getHeight() / 4.0f);
            if (cell.getActor() instanceof TextButton) {
                ((TextButton) cell.getActor()).getLabel().setFontScale(table.getScaleX(), table.getScaleY());
                }
            if (cell.getActor() instanceof Label) {
                ((Label) cell.getActor()).setFontScale(table.getScaleX(), table.getScaleY());
            }
            if(cell.getActor() instanceof Image)
                cell.size(width / 4.0f, height / 4.0f);
            if(cell.getActor() instanceof Table) {
                resizeNestedTable(table, (Table) cell.getActor(), width, height);
            }
        }
    }

    private void resizeNestedTable(Table parent, Table table, int width, int height) {
        for (Cell cell : table.getCells()) {
            cell.size(parent.getWidth(), parent.getHeight() / 4.0f);
            if (cell.getActor() instanceof TextButton) {
                ((TextButton) cell.getActor()).getLabel().setFontScale(parent.getScaleX(), parent.getScaleY());
            }
            if (cell.getActor() instanceof Label) {
                ((Label) cell.getActor()).setFontScale(parent.getScaleX(), parent.getScaleY());
                cell.size(width/1.5f, height / 4.0f);
            }
            if (cell.getActor() instanceof Image)
                cell.size(width / 4.0f, height / 4.0f);
        }
    }
}

