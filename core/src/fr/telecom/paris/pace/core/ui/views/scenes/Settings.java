package fr.telecom.paris.pace.core.ui.views.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import fr.telecom.paris.pace.core.ui.Assets;
import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.PaceClient;

public class Settings extends ViewWithPrevious {

    private Sound buttonSound;
    private final Label musicVolumeLabel;
    private final Label soundVolumeLabel;

    private final Button back;

    private final Slider musicVolume;

    private final Slider soundVolume;

    private final Label fullScreenLabel;

    private final CheckBox fullScreen;

    private Camera camera;

    private Table table;

    private final ScrollPane pane;
    public Settings(View previous, Controller controller, Model model, PaceClient pace) {
        super(previous, controller, model, pace);
        Skin skin = getAssets().getSkin();
        buttonSound = Assets.getInstance().getSound();
        musicVolumeLabel = new Label(Assets.getInstance().getText("music_volume"), skin);
        soundVolumeLabel = new Label(Assets.getInstance().getText("sound_volume"), skin);
        back = new TextButton(Assets.getInstance().getText("back"), skin);
        musicVolume = new Slider(0, 100, 10, false, skin);
        soundVolume = new Slider(0, 100, 10, false, skin);
        fullScreenLabel = new Label(Assets.getInstance().getText("fullscreen"), skin);
        fullScreen = new CheckBox("", skin);
        fullScreen.setChecked(getClient().isFullScreen());
        musicVolume.setValue(getClient().getMusicVolume() * 100);
        soundVolume.setValue(getClient().getSoundVolume() * 100);
        pane = new ScrollPane(null, skin);

    }

    @Override
    public void init() {
        setBackground(new Texture(Gdx.files.internal("background/doors.jpeg")));
        setBackground(Assets.getInstance().get(Assets.AssetDir.BACKGROUND.path() + "doors.jpeg", Texture.class));
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Viewport viewport = new ScreenViewport(camera);
        viewport.apply();
        initListener();
        setStage(viewport);
        table = new Table();
        table.add(drawTable()).expand().center();
        table.row();
        table.add(back).expandX().center().pad(30);
        table.setFillParent(true);
        addActor(table);
        Gdx.input.setInputProcessor(getStage());
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        camera.update();
        getBackground().setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        table.setSize(width / 4.0f, height / 4.0f);
        resizeTable(table, width, height);
    }

    private void resizeTable(Table table, int width, int height) {
        table.setSize(width / 4.0f, height / 4.0f);
        for (Cell cell : table.getCells()) {
            cell.size(table.getWidth(), table.getHeight() / 4.0f);
            if (cell.getActor() instanceof TextButton) {
                ((TextButton) cell.getActor()).getLabel().setFontScale(table.getScaleX(), table.getScaleY());
            }
            if (cell.getActor() instanceof Label) {
//                ((Label) cell.getActor()).setFontScale(table.getScaleX(), table.getScaleY());
            }
            if(cell.getActor() instanceof Image)
                cell.size(width / 4.0f, height / 4.0f);
            if(cell.getActor() instanceof Table) {
                resizeNestedTable(table, (Table) cell.getActor(), width, height);
            }
        }
    }

    private void resizeNestedTable(Table parent, Table table, int width, int height) {
        for (Cell cell : table.getCells()) {
            cell.size(parent.getWidth(), parent.getHeight() / 4.0f);
            if (cell.getActor() instanceof TextButton) {
                ((TextButton) cell.getActor()).getLabel().setFontScale(parent.getScaleX(), parent.getScaleY());
            }
            if (cell.getActor() instanceof Label) {
                ((Label) cell.getActor()).setFontScale(parent.getScaleX(), parent.getScaleY());
                cell.size(width/1.5f, height / 4.0f);
            }
            if (cell.getActor() instanceof Image)
                cell.size(width / 4.0f, height / 4.0f);
        }
    }

    @Override
    public void draw(float delta) {
        getBatch().setTransformMatrix(camera.view);
        getBatch().setProjectionMatrix(camera.projection);
        getBatch().begin();
//        getBatch().setColor(0.5f,0.5f, 0.5f, 1f);
        getBackground().setColor(0.2f, 0.2f, 0.2f, 1f);
        getBackground().draw(getBatch());
//        getBatch().setColor(1,1,1, 1f);
        getBatch().end();
        getStage().draw();
    }

    private Table drawTable() {
        Table table = new Table();
        float padR = 30f;
        float padL = 10f;
        float padC = 10;
        Label empty = new Label(
                "", Assets.getInstance().getSkin()
        );
        table.add(fullScreenLabel).padRight(padR).left();
        table.add(fullScreen).center().pad(padC);
        table.row();
//        table.add(musicVolumeLabel).padRight(padR).left();
//        table.add(musicVolume).center().pad(padC);
//        table.row();
        table.add(soundVolumeLabel).padRight(padR).left();
        table.add(soundVolume).center().pad(padC);

        return table;
    }

    /**
     * Init the buttons' listener.
     */
    private void initListener() {
        back.addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        getController().playSound(buttonSound);
                        getController().back(getPrevious());

                    }
                }
        );

        musicVolume.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getController().playSound(buttonSound);
                getClient().setMusicVolume(musicVolume.getValue() / 100);

            }
        });
        soundVolume.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getController().playSound(buttonSound);
                getClient().setSoundVolume(soundVolume.getValue() / 100);

            }
        });

        fullScreen.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getController().playSound(buttonSound);
                getClient().setFullScreen(fullScreen.isChecked());

            }
        });
    }
}
