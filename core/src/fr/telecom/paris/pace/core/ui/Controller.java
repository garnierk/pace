package fr.telecom.paris.pace.core.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.Timer;
import fr.telecom.paris.pace.core.ui.views.objects.DialogWithCloseButton;
import fr.telecom.paris.pace.core.ui.views.scenes.*;

import java.util.ArrayList;
import java.util.Date;

import fr.telecom.paris.pace.core.utils.NetUtils;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;

public class Controller {

    private Model model;
    private PaceClient pace;

    /**
     * The screen to control.
     */
    private View screen;

    public Controller(Model model, PaceClient paceClient, View screen) {
        this.model = model;
        this.pace = paceClient;
    }

    public Controller(Model model, PaceClient paceClient) {
        this(model, paceClient, null);
    }

    public void create() {
        model = new Model();
    }

    public void startGame() {
        create();
        getModel().startGame();
        GameView view = new GameView(this, getModel(), getClient());
        setScreen(view);

    }

    public void openSettings() {
        Model model = getModel();
        getClient().getAssets().load();
        setScreen(new Settings(getScreen(), this, model, getClient()));
    }

    public void back(View previous) {
        setScreen(previous);
    }

    public Model getModel() {
        return model;
    }

    public void playSound(Sound sound) {
        if (sound != null) {
            sound.play(getClient().getSoundVolume());
        }
    }

    public PaceClient getClient() {
        return pace;
    }

    public View getScreen() {
        return screen;
    }

    public void setScreen(View screen) {
        this.screen = screen;
        getClient().setScreen(screen);
    }

    public void openMainMenu() {
        setScreen(new MainMenu(this, getModel(), getClient()));
    }

    public void splashScreen() {
    }

    /**
     * Stop the game.
     */
    public void stopGame(boolean save) {
        if (save)
            saveGame();
        getModel().dispose();
    }

    /**
     * close the client.
     */
    public void closeClient() {
        Gdx.app.exit();
    }

    public void openInGameMenu() {
        setScreen(new InGameMenu(getScreen(), this, getModel(), getClient()));
    }

    public void nextYear() {
        if (getModel().isGameOver()) {
            destroySave();
            setScreen(new GameOverScreen(this, getModel(), getClient()));
            sendDataToServer(getModel().getPlayerEndState());
            return;
        }
        getModel().nextYear();
        switchScreen();

    }

    public void nextYear(Model.EventChoices choice) {
        if (getModel().isGameOver()) {
            destroySave();
            setScreen(new GameOverScreen(this, getModel(), getClient()));
            sendDataToServer(getModel().getPlayerEndState());
            return;
        }
        getModel().nextYear(choice);
        switchScreen();
    }

    public void saveGame() {
        Preferences prefs = Gdx.app.getPreferences("pace");
        Json json = new Json();
        prefs.putString("game", json.toJson(getModel()));
        prefs.putInteger("year", getModel().getYear());
        Date date = new Date();
    String dateString = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getYear();
        prefs.putString("date", dateString);
        prefs.flush();

    }

    public void loadGame() {
        Preferences prefs = Gdx.app.getPreferences("pace");
        Json json = new Json();
        this.model = json.fromJson(Model.class, prefs.getString("game"));
        getModel().load_story();
        GameView view = new GameView(this, getModel(), getClient());
        setScreen(view);
    }

    public void sendDataToServer(int playerEndState) {
        // Create a new JsonValue
        JsonValue jsonValue = new JsonValue(JsonValue.ValueType.object);

        // Convert ArrayList to JsonValue array and add randomIds and majorIds from the Model to the JsonValue;
        jsonValue.addChild("ids", convertListToJsonValueArray(new ArrayList<>(model.getEventsDone())));
        // Add playerEndState to the JsonValue
        jsonValue.addChild("end_state", new JsonValue(playerEndState));

        // Send the JsonValue to the server
        NetUtils.sendJsonToServer(jsonValue);
    }

    private JsonValue convertListToJsonValueArray(ArrayList<Integer> list) {
        JsonValue array = new JsonValue(JsonValue.ValueType.array);
        for (Integer item : list) {
            array.addChild(new JsonValue(item));
        }
        return array;
    }

    public void receiveDataFromServer() {
        // TODO
    }


    private void switchScreen() {
        GameView gameView = (GameView) getScreen();
        View yearScreen = new YearScreen(this, getModel(), getClient());
        FadeScreen.FadeInfo fadeOut = new FadeScreen.FadeInfo(FadeScreen.FadeType.OUT, Color.BLACK, Interpolation.smoother, 2.0f);
        FadeScreen.FadeInfo fadeIn = new FadeScreen.FadeInfo(FadeScreen.FadeType.IN, Color.BLACK, Interpolation.smoother, 2.0f);

//        switchScreen(new YearScreen(this, getModel(), getClient()));

//        switchScreen(gameView);
        FadeScreen transtition = new FadeScreen(this, getModel(), getClient(), gameView, yearScreen, fadeOut);
        getClient().setScreen(transtition, false);
        Controller controller = this;
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                gameView.setYears(getModel().getYearString());
                getClient().setScreen(new FadeScreen(controller, getModel(), getClient(), yearScreen, gameView, fadeOut), false);
                gameView.showDocument();
            }
        }, 3);
    }

    public void openDocument() {

    }

    public void applyChoice(Model.EventChoices choice) {
        nextYear(choice);
    }

    public void dialogClosed(DialogWithCloseButton dialog) {

        Model.Event current = getModel().getCurrentEvent();
        if (!current.hasChoices()) {
            dialog.hide();
            if (current.getType() == Model.EventTypes.ANSWER) {
                loadNewEvent();
                if (getModel().isGameOver()) {
                    nextYear();
                }
                if (getScreen() instanceof GameView)
                    ((GameView) getScreen()).updateBars();
            } else {
                nextYear();
            }
        }
    }

    private void loadNewEvent() {
        getModel().loadNewEvent();
        ((GameView)getScreen()).showDocument();
    }

    public void showLoadScreen() {
        Preferences prefs = Gdx.app.getPreferences("pace");
        if (prefs.contains("game") && prefs.contains("year") && prefs.contains("date")){
            String date = prefs.getString("date");
            int year = prefs.getInteger("year");
            setScreen(new LoadScreen(this, getModel(), getClient(), true, date, year));
        } else {
            setScreen(new LoadScreen(this, getModel(), getClient()));
        }
    }

    private void destroySave() {
        Preferences prefs = Gdx.app.getPreferences("pace");
        prefs.remove("game");
        prefs.remove("year");
        prefs.remove("date");
        prefs.flush();
    }
}
