package fr.telecom.paris.pace.core.ui.views.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import fr.telecom.paris.pace.core.ui.Assets;
import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.PaceClient;

public class InGameMenu extends ViewWithPrevious {

    private Sprite background;

    /**
     * Initialize the screen.
     *
     * @param controller The screen controller.
     * @param model      The gui model.
     * @param pace  The client.
     */
    public InGameMenu(View screen, Controller controller, Model model, PaceClient pace) {
        super(screen, controller, model, pace);
    }

    private TextButton close;

    private TextButton quit;

    private TextButton settings;

    private TextButton mainMenu;

    private Camera camera;

    private Table table;


    /**
     * Initialize the screen and its components.
     */
    @Override
    public void init() {
        Skin skin = Assets.getInstance().getSkin();
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        setStage(new ScreenViewport(camera));
        close = new TextButton("Return", skin);
        mainMenu = new TextButton("Main Menu", skin);
        quit = new TextButton("Quit", skin);
        settings = new TextButton("Settings", skin);
        addActor(initTable());
        initListener();
        setBackground(Assets.getInstance().get(Assets.AssetDir.BACKGROUND.path() + "dictator_office.jpeg", Texture.class));
    }

    private Table initTable() {
        table = new Table();
        table.setFillParent(true);
        table.add(close).expand();
        table.row();
        table.add(settings).expand();
        table.row();
        table.add(mainMenu).expand();
        table.row();
        table.add(quit).expand();
        return table;
    }

    private void initListener(){
        close.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getController().playSound(Assets.getInstance().getSound());
                getController().back(getPrevious());
            }
        });
        settings.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getController().playSound(Assets.getInstance().getSound());
                getController().openSettings();
            }
        });

        mainMenu.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getController().playSound(Assets.getInstance().getSound());
                getController().stopGame(true);
                getController().openMainMenu();
            }
        });

        quit.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getController().playSound(Assets.getInstance().getSound());
                getController().closeClient();
            }
        });
    }

    /**
     * Draw the screen.
     *
     * @param delta The number of second that have passed since the last frame
     */
    @Override
    public void draw(float delta) {
        getBatch().setTransformMatrix(camera.view);
        getBatch().setProjectionMatrix(camera.projection);
        getBatch().begin();
//        getBatch().setColor(0.5f,0.5f, 0.5f, 1f);
        getBackground().setColor(0.5f,0.5f, 0.5f, 1f);
        getBackground().draw(getBatch());
//        getBatch().setColor(1,1,1, 1f);
        getBatch().end();
        getStage().draw();

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        table.setSize(width / 4.0f, height / 4.0f);
        for(Cell cell : table.getCells()) {
            cell.size(table.getWidth(), table.getHeight() / 4.0f);
            if (cell.getActor() instanceof TextButton)
                ((TextButton) cell.getActor()).getLabel().setFontScale(table.getScaleX(), table.getScaleY());
        }

        camera.update();
//        table.scaleBy(scale);
        getBackground().setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }
}