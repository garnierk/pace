package fr.telecom.paris.pace.core.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import fr.telecom.paris.pace.core.utils.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class Assets {
    private static final char fs = File.separatorChar;
    /**
     * Asset manifest extension.
     */
    private static final String asmext = ".asman";

    /**
     * The app asset manager instance.
     */
    private static Assets instance = new Assets();

    /**
     * The assets manager.
     */
    private final AssetManager manager;

    /**
     * The game descriptions.
     */
    private final Map<Object, FileHandle> descriptions;

    private String language = "fr";

    private JsonReader reader = new JsonReader();

    private JsonValue stringsRoot;


    private Assets() {
        manager = new AssetManager();

        descriptions = new HashMap<>();
    }

    public static Assets getInstance() {
        return instance;

    }

    /**
     * Loads the assets.
     */
    public void load() {
        loadAllAssets();
        printLoading();
    }

    /**
     * Loads all the assets in the Asset dir given in parameter.
     *
     * @param dir The assetDir constant that contains the assets to load.
     */
    public void load(AssetDir dir) {
        fileLoader(dir);
        printLoading();
    }

    /**
     * Loads a single file.
     *
     * @param path    The file path
     * @param type    The assets type (Texture.class, etc.)
     * @param isAlone if true, the asset manager will load file,
     *                else the manager will put the file in queue waiting to be updated. <br>
     *                <b>You must call printLoading() to load all queued files. </b>
     */
    public void load(String path, Class<?> type, boolean isAlone) {
        manager.load(path, type);
        if (isAlone) {
            printLoading();
        }
    }


    /**
     * Loads all the assets in the manifest.
     *
     * @param manifestPath The manifest listing the assets to load
     * @param isAlone      if true, the asset manager will load file,
     *                     else the manager will put the file in queue waiting to be updated. <br>
     *                     <b>You must call printLoading() to load all queued files. </b>
     */
    public void load(String manifestPath, boolean isAlone) {
        FileHandle fileLoader = Gdx.files.internal(manifestPath);
        if (fileLoader.extension().equals(asmext)) {
            fileLoader(fileLoader.parent().path(), fileLoader.name());
        }
        if (isAlone) {
            printLoading();
        }
    }

    /**
     * Load all the game assets.
     */
    private void loadAllAssets() {
        for (AssetDir dir : AssetDir.values()) {
            fileLoader(dir);
        }
    }

    /**
     * Gets the game main skin.
     *
     * @return the main Skin.
     */
    public Skin getSkin() {
        return get(AssetDir.SKIN.path  +"skin.json", Skin.class);
    }

    /**
     * Gets the game button sounds
     *
     * @return The sound.
     */
    public Sound getSound() {
        return get(AssetDir.SOUND.path + "switch.wav");
    }

    /**
     * Print the loading.
     */
    public void printLoading() {
        while (!manager.update()) {
            float progress = manager.getProgress();
            Log.print("Loading ... " + progress * 100 + "%");
        }
        Log.print("Loading ... " + 100.0 + "%");
    }

    /**
     * Load the assets in the AssetManager.
     *
     * @param dirPath  The directory path.
     * @param manifest The manifest name with extension
     */
    private void fileLoader(String dirPath, String manifest) {
        if (manifest.isEmpty()) {
            return;
        }
        try {
            FileHandle fileLoader = Gdx.files.internal(dirPath + fs + manifest);
            Scanner scanner = new Scanner(fileLoader);
            Class<?> c = AssetType.valueOf(scanner.nextLine()).type;
            if (c.equals(Skin.class)) {
                loadSkin(dirPath, scanner);
            } else {

                while (scanner.hasNextLine()) {
                    String path = dirPath + scanner.nextLine();
                    manager.load(path, c);
                }
            }
            scanner.close();
        } catch (Exception ignored) {
        }
    }

    /**
     * Load the assets in the AssetManager.
     *
     * @param dir The constant linked to an asset dir.
     */
    private void fileLoader(AssetDir dir) {
        if (dir.manifest == null) {
            return;
        }
        for (String s : dir.manifest) {
            fileLoader(dir.path, s);
        }
    }

    /**
     * Loads a skin from the manifest represented by the dirPath.
     *
     * @param dirPath The manifest path.
     * @param scanner the loader scanner.
     */
    private void loadSkin(String dirPath, Scanner scanner) {
        while (scanner.hasNextLine()) {
            String path = dirPath + scanner.nextLine();
            manager.load(path, Skin.class, new SkinLoader.SkinParameter());
        }
    }

    /**
     * Gets the fileName's asset.
     *
     * @param fileName the asset file name.
     * @return the asset.
     * @throws GdxRuntimeException if the asset is not loaded.
     */
    public <T> T get(String fileName) {
        fileName = fileName.replace('\\', '/');
        return manager.get(fileName);
    }


    /**
     * Gets the fileName's asset with the specified type.
     *
     * @param fileName the asset file name.
     * @param type     the asset type.
     * @return the asset.
     * @throws GdxRuntimeException if the asset is not loaded.
     */
    public <T> T get(String fileName, Class<T> type) {
        fileName = fileName.replace('\\', '/');
        return manager.get(fileName, type);
    }

    /**
     * Gets the fileName's asset.
     *
     * @param fileName the asset file name.
     * @param required true to throw GdxRuntimeException if the asset is not loaded, else null is returned.
     * @return the asset or null if it is not loaded and required is false.
     */
    public <T> T get(String fileName, boolean required) {
        fileName = fileName.replace('\\', '/');
        return manager.get(fileName, required);
    }

    /**
     * Gets the fileName's asset.
     *
     * @param fileName the asset file name.
     * @param type     the asset type.
     * @param required true to throw GdxRuntimeException if the asset is not loaded, else null is returned.
     * @return the asset or null if it is not loaded and required is false.
     */
    public <T> T get(String fileName, Class<T> type, boolean required) {
        fileName = fileName.replace('\\', '/');
        return manager.get(fileName, type, required);
    }


    /**
     * Gets the description of the given type.
     *
     * @param type       The entity type.
     * @param lineLength The length of the lines.
     * @return the description as a String. Each line is of length lineLength.
     * @throws FileNotFoundException if the description wasn't loaded
     */
    public String get(Object type, int lineLength) throws FileNotFoundException {
        if (lineLength < 1) {
            return "";
        }
        FileHandle f = descriptions.get(type);
        if (f == null) {
            throw new RuntimeException("The asset was not loaded for the Entity type");
        }
        return readFile(f, lineLength);
    }

    /**
     * Reads the description file.
     *
     * @param file       The file which wil be read.
     * @param lineLength The length of the line in the return string.
     * @return The file content, formatted according to the given length.
     */
    private String readFile(FileHandle file, int lineLength) {
        Scanner scanner = new Scanner(file);
        StringBuilder builder = new StringBuilder();
        int len = 0;
        while (scanner.hasNextLine()) {
            String[] line = scanner.nextLine().split(" ");
            for (String l : line) {
                if (len + l.length() >= lineLength) {
                    builder.append('\n');
                    len = 0;
                }
                builder.append(l).append(' ');
                len += l.length() + 1;
            }
            builder.append('\n');
        }
        scanner.close();
        return builder.toString();
    }

    /**
     * Gets all the assets.
     *
     * @param type the asset type.
     * @return all the assets matching the specified type.
     */
    public <T> Array<T> getAll(Class<T> type, Array<T> out) {
        return manager.getAll(type, out);
    }

    /**
     * Dispose the Assets.
     */
    public void dispose() {
        manager.dispose();
        instance = null;
        descriptions.clear();

    }

    public String getText(String key) {
        return stringsRoot.getString(key);
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public JsonValue getStory() {
        return reader.parse(Gdx.files.internal(AssetDir.STORY.path+"story-"+language+".json"));
    }

    public void loadString() {
        stringsRoot = reader.parse(Gdx.files.internal(AssetDir.STORY.path+"strings-"+language+".json"));
    }

    public void waitLoading() {
        while (!manager.update()) {
            Log.print("Loading ... " + manager.getProgress() * 100 + "%");
        }
    }


    /**
     * List the assets directories and their manifest.
     */
    public enum AssetDir {
        BACKGROUND("background/", "background"),
        SKIN("skin/", "skin"),
        SOUND("sound/", "sound"),
        MUSIC("music/", "music"), STORY("story/", "story"),
        ICONS("icons/", "icons");
        /**
         * the directory path.
         */
        private final String path;
        /**
         * The directory manifest.
         */
        private final String[] manifest;

        AssetDir(String path, String... manifest) {
            this.path = path;
            this.manifest = manifest;
            if (manifest != null) {
                for (int i = 0; i < manifest.length; i++) {
                    if (!manifest[i].isEmpty()) {
                        manifest[i] += asmext;
                    }
                }
            }
        }

        AssetDir(String path) {
            this(path, "");
        }

        /**
         * Gets the directory path.
         *
         * @return the directory path.
         */
        public String path() {
            return path;
        }

        public String[] getManifest() {
            return manifest;
        }
    }

    /**
     * List the Assets Types.
     */
    public enum AssetType {
        TEXTURE(Texture.class), SKIN(Skin.class), SOUND(Sound.class), ATLAS(TextureAtlas.class), MUSIC(Music.class);

        private final Class<?> type;

        AssetType(Class<?> type) {
            this.type = type;
        }
    }

    private static class Scanner {
        FileHandle file;
        BufferedReader reader;
        String line = null;

        public Scanner(String s) {
            this(new FileHandle(s));
        }

        public Scanner(FileHandle file) {
            this.file = file;
            try {
                reader = new BufferedReader(file.reader());
            } catch (Exception e) {
                throw new GdxRuntimeException("Couldn't load file: " + file);
            }
        }

        public void close() {
        }

        public boolean hasNextLine() {
            try {
                line = reader.readLine();
            } catch (Exception e) {
                throw new GdxRuntimeException("Couldn't read file: " + file);
            }
            return line != null;
        }

        public String nextLine() {
            if (line != null) {
                String ret = line;
                line = null;
                return ret;
            } else {
                try {
                    return reader.readLine();
                } catch (Exception e) {
                    throw new GdxRuntimeException("Couldn't read file: " + file);
                }
            }
        }
    }
}

