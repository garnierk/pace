package fr.telecom.paris.pace.core.ui.views.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import fr.telecom.paris.pace.core.ui.Assets;
import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.PaceClient;
import fr.telecom.paris.pace.core.ui.views.scenes.View;

import java.util.ArrayList;

public class SecretDocumentDialog extends DialogWithCloseButton {

    SecretDocument document;
    ScrollPane pane;
    Camera camera;
    TextButton close;
    private Label title;
    private Label subject;
    private Label date;
    private Label content;
    private Table table;
    boolean visible = false;

    public SecretDocumentDialog(Controller controller) {
        super("Dialo",controller);
    }


    @Override
    public void init() {
        pane = initialisePanel(createTable());
        getContentTable().add(pane).expand().top().fill();
        getContentTable().row();
        getContentTable().add().expand().fill();
        getContentTable().row();
        getContentTable().add(create_buttons()).expand().fill();
//        document.update(getController().getModel());
        getBackground().setMinWidth(0);
        setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        this.setSkin(Assets.getInstance().get(Assets.AssetDir.SKIN.path() + "ui.json", Skin.class));
    }

    @Override
    public float getPrefHeight() {
        return Math.max(Gdx.graphics.getHeight() / 1.5f, 300);
    }

    public float getPrefWidth() {
        return Math.max(Gdx.graphics.getWidth() / 2, 500);
    }


    public void initInput() {
    }

    private Table createTable() {
        Skin skin = Assets.getInstance().getSkin();

        title = new Label(Assets.getInstance().getText("document_title"), skin);
        subject = new Label(Assets.getInstance().getText("document_subject") + " : ", skin);
        date = new Label(Assets.getInstance().getText("document_date") + " : ", skin);
        content = new Label(Assets.getInstance().getText("document_content") + " : ", skin);
        content.setWrap(true);
        subject.setWrap(true);
        // Underline the subject
        subject.getStyle().font.getData().markupEnabled = true;
        date.setColor(0, 0, 0, 1);
        content.setColor(0, 0, 0, 1);
        title.setColor(200, 0, 0, 1);
        subject.setColor(0, 0, 0, 1);



        table = new Table();

        // Add the title, subject, date, and content to the table
        table.add(title).expandX().pad(20).top();
        table.row();
        table.add(subject).expandX().pad(10).left().fill().top();
        table.row();
        table.add(date).expandX().pad(10).left().fill().top();
        table.row();
        table.add().expandX().pad(10).top();
        table.row();

        table.add(content).expandX().fill().pad(10).top();

        float fontScale = 1.5f;
        float titleScale = 1.2f;
        if (Gdx.graphics.getWidth() / 800 > 2 || Gdx.graphics.getWidth() / 600 > 2) {
            fontScale = 1.2f;
            titleScale = 1;
        }
        title.setFontScale( table.getScaleX() / titleScale,table.getScaleY()/ titleScale);
        subject.setFontScale( table.getScaleX()/ fontScale,table.getScaleY()/ fontScale);
        date.setFontScale(table.getScaleX()/ fontScale,table.getScaleY()/ fontScale);
        content.setFontScale(table.getScaleX()/ fontScale,table.getScaleY()/ fontScale);
        return table;
    }

    public void update() {
        subject.setText(Assets.getInstance().getText("document_subject") + " : " + getController().getModel().getSubject());
        date.setText(Assets.getInstance().getText("document_date")+ " : " + getController().getModel().getDate());
        content.setText(getController().getModel().getContent());
    }

    private ScrollPane initialisePanel(Table table) {
        ScrollPane pane = new ScrollPane(table, Assets.getInstance().getSkin());
        pane.setScrollbarsVisible(false);
        pane.setScrollingDisabled(true, false);
        pane.setScrollbarsVisible(true);
        pane.setCancelTouchFocus(false);
        pane.setSmoothScrolling(true);
        return pane;
    }

    public void resize(int width, int height) {
//        document.resize(width, height);
        float scaleY = 1.5f;
        float scaleX = 2f;
        float screenScale = (float) width / height;
        if (screenScale >= 1.75 && screenScale <= 1.8) {
            scaleX = 3 *   1.77f * 1.44f;
            scaleY *= 1.77f;
        }
        setSize(width/scaleX, height/scaleY);
        setPosition(width /2f - getWidth()/2f ,height/2f - getHeight()/2f);
        float fontScale = 1.5f;
        float titleScale = 1.2f;
        if (Gdx.graphics.getWidth() / 800 > 2 || Gdx.graphics.getWidth() / 600 > 2) {
            fontScale = 1.2f;
            titleScale = 1;
        }

        title.setFontScale( table.getScaleX() / titleScale,table.getScaleY()/ titleScale);
        subject.setFontScale( table.getScaleX()/ fontScale,table.getScaleY()/ fontScale);
        date.setFontScale(table.getScaleX()/ fontScale,table.getScaleY()/ fontScale);
        content.setFontScale(table.getScaleX()/ fontScale,table.getScaleY()/ fontScale);


    }

    public Table create_buttons() {
        Skin skin = Assets.getInstance().getSkin();
        Table table = new Table();
        ArrayList<Model.EventChoices> choices = getController().getModel().getChoices();
        for (Model.EventChoices choice : choices) {
            TextButton button = new TextButton(choice.getText(), skin);
            button.getLabel().setWrap(true);
            button.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    getController().applyChoice(choice);
//                    getController().dialogClosed();
                    hide();
                }
            });
            table.add(button).expandX().fillX().pad(10);
            table.row();
        }

        return table;
    }
}
