package fr.telecom.paris.pace.core.ui.views.scenes;

import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.PaceClient;

/**
 * This is a view who manage a back buttons.
 */
public abstract class ViewWithPrevious extends View {
    /**
     * The previous screen.
     */
    private final View previous;

    /**
     * Constructor for screen.
     *
     * @param controller The screen controller.
     * @param model      The gui model.
     * @param pace       The client.
     */
    public ViewWithPrevious(View previous, Controller controller, Model model, PaceClient pace) {
        super(controller, model, pace);
        this.previous = previous;
    }

    /**
     * Getter for the previous view
     *
     * @return the previous view
     */
    public View getPrevious() {
        return previous;
    }
}
