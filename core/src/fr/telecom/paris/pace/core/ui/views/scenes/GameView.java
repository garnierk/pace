package fr.telecom.paris.pace.core.ui.views.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import fr.telecom.paris.pace.core.ui.Assets;
import fr.telecom.paris.pace.core.ui.Controller;
import fr.telecom.paris.pace.core.ui.Model;
import fr.telecom.paris.pace.core.ui.PaceClient;
import fr.telecom.paris.pace.core.ui.views.objects.HealthBar;
import fr.telecom.paris.pace.core.ui.views.objects.SecretDocument;
import fr.telecom.paris.pace.core.ui.views.objects.SecretDocumentDialog;
import fr.telecom.paris.pace.core.utils.Drawer;

public class GameView extends View{
    private TextButton menuButton;

    private Label years;

    private Table table;

    private Camera camera;

    private SecretDocumentDialog documentDialog;

    private ProgressBar stability;
    private ProgressBar liberty;
    private ProgressBar propaganda;

    private Drawable tableBackground;



    public GameView(Controller controller, Model model, PaceClient pace) {
        super(controller, model, pace);
    }



    @Override
    public void init() {
        initGameViewUI();
        initInput();
        setBackground(Assets.getInstance().get(Assets.AssetDir.BACKGROUND.path() + "dictator_office.jpeg", Texture.class));
    }

    private void initInput() {
        menuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getController().playSound(Assets.getInstance().getSound());
                getController().openInGameMenu();
            }
        });
        menuButton.addListener(new InputListener(){
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (x < menuButton.getX() || y < menuButton.getY() || x > menuButton.getX() + menuButton.getWidth() || y > menuButton.getY() + menuButton.getHeight()) {
                    getController().playSound(Assets.getInstance().getSound());
                    getController().openInGameMenu();
                    System.out.println("x: " + x + " y: " + y + " button: " + button + " pointer: " + pointer + " menuButton: " + menuButton.getX() + " " + menuButton.getY() + " " + menuButton.getWidth() + " " + menuButton.getHeight());
                    return true;
                }
                return false;

            }
        });
    }

    private void initGameViewUI() {
        // Create the game view UI
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        menuButton = new TextButton(getAssets().getText("menu"), getAssets().getSkin());
        years = new Label(getModel().getYearString(), getAssets().getSkin());


        Viewport viewport = new ScreenViewport(camera);
        setStage(viewport);
        table = new Table();
        table.setFillParent(true);
        table.top().left();


        // Attach menuButton to the top left of the screen
        table.add(menuButton).top().left().expandX().pad(10);

        // Attach years label to the top right of the screen
        table.add(years).top().right().expandX().pad(10);
        table.row();
        table.add();
        table.add(create_health_ui()).top().right().expandX().pad(10);


        // Move to the next row and expand it to push the buttons to the top
        table.row().expand().fill();
        table.row().expand().fill();


        addActor(table);

        addActor(table);
        addInput(getStage());
        InputMultiplexer input = getInputs();
        documentDialog = new SecretDocumentDialog(getController());
        documentDialog.show(getStage());
        addInput(menuButton.getStage());

    }

    private Table create_health_ui() {
        Table table = new Table();
        tableBackground = Drawer.getColoredDrawable(128,128,Color.WHITE);
        table.setBackground(tableBackground);

        stability = new HealthBar(100, 30, getModel().getOldStability(), getModel().getStability());
        liberty = new HealthBar(100, 30, getModel().getOldLiberty(), getModel().getLiberty());
        propaganda = new HealthBar(100, 30, getModel().getOldPropaganda(), getModel().getPropaganda());
        Image stabilityIcon = new Image(Assets.getInstance().get(Assets.AssetDir.ICONS.path() + "stability.png", Texture.class));
        Image libertyIcon = new Image(Assets.getInstance().get(Assets.AssetDir.ICONS.path() + "liberty.png", Texture.class));
        Image propagandaIcon = new Image(Assets.getInstance().get(Assets.AssetDir.ICONS.path() + "propaganda.png", Texture.class));
        table.add(stabilityIcon).top().right().expandX().padTop(10).size(48,48);
        table.add(stability).top().right().expandX().pad(10).size(64,48);;
        table.row();
        table.add(libertyIcon).top().right().expandX().padTop(10).size(48,48);;
        table.add(liberty).top().right().expandX().pad(10).size(64,48);;
        table.row();
        table.add(propagandaIcon).top().right().expandX().padTop(10).size(48,48);;
        table.add(propaganda).top().right().expandX().pad(10).size(64,48);;
        return table;

    }

    @Override
    public void draw(float delta) {
        getBatch().setTransformMatrix(camera.view);
        getBatch().setProjectionMatrix(camera.projection);
        getBatch().begin();
        getBackground().draw(getBatch());
        getBatch().end();
        getStage().act(delta);
        getStage().draw();
//        documentDialog.draw(delta);

    }

    public void setYears(String years) {
        this.years.setText(years);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        table.setSize(width / 4.0f, height / 4.0f);
        for(Cell cell : table.getCells()) {
            if (cell != null && cell.getActor() instanceof  TextButton)
                ((TextButton) cell.getActor()).getLabel().setFontScale(table.getScaleX(), table.getScaleY());
            if (cell != null && cell.getActor() instanceof Table) {
                Table subTable = (Table) cell.getActor();
                resizeTable(width, height, subTable);
            }
        }
        years.setFontScale(table.getScaleX(), table.getScaleY());
        camera.update();
        getBackground().setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        documentDialog.resize(width, height);
    }

    private void resizeTable(int width, int height, Table table) {

//        for (Cell cell : table.getCells()) {
//            if (cell != null && cell.getActor() instanceof Image)
//                cell.getActor().setSize(48 * scale, 48 * scale);
//            if (cell != null && cell.getActor() instanceof HealthBar)
//                cell.getActor().setSize( 32 * scale, 48 * scale);
//        }

    }

    public void showDocument() {
        documentDialog = new SecretDocumentDialog(getController());
        documentDialog.show(getStage());
    }

    public void updateBars() {
        stability.setValue(getModel().getStability());
        liberty.setValue(getModel().getLiberty());
        propaganda.setValue(getModel().getPropaganda());
    }

    public TextButton getMenuButton() {
        return menuButton;
    }
}
